include apt


# 
#
#



########## PACKAGES ##########

$install_packages = [
	'tree',
	'pktstat',
	'openjdk-8-jdk',
	'vim',
	'vifm',
	'tmux',
	'openssh-server',
	'openssh-client',
	'openssl',
	'cryptsetup',
	'curl',
	'wget',
	'nodejs',
	'lsof',
	'python3',
	'python3-pip',
	'python-boto3',
	'virtualenv',
	'python3-venv',
	'pip3',
	]

$remove_packages = ['nano','emacs', ]

node 'default' {
    package { $install_packages:
        ensure => 'present'
    }

    package { $remove_packages:
        ensure => 'absent'
    }
}


########## SUDO ##########
$sudoers_file = '
# Managed by puppet, do not edit
#
# Changes from default:
#   timestamp_timeout
#   root has NOPASSWD

Defaults	env_reset,timestamp_timeout=30
Defaults	mail_badpass
Defaults	secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
root	ALL=(ALL:ALL) NOPASSWD:ALL
%sudo	ALL=(ALL:ALL) ALL
#includedir /etc/sudoers.d
'

file { "/etc/sudoers":
    #source 	=> "file:///etc/puppet/code/xubuntu1804ns/modules/ubuntu_base/files/sudoers",	
    content		=> $sudoers_file,
    owner 		=> "root",
    group		=> "root",
    mode  		=> '0640',
    backup 		=> 'false',
}

########## VIM ##########
$vimrc_file = 'set mouse-=a
'

file { "/etc/skel/.vimrc":
    #source 	=> "file:///etc/puppet/code/xubuntu1804ns/modules/ubuntu_base/files/sudoers",	
    content		=> $vimrc_file,
    owner 		=> "root",
    group		=> "root",
    mode  		=> '0644',
    backup 		=> 'false',
}



########## ADMIN USER ##########
$admin_user = "groot"

user { $admin_user:
	name				=>	$admin_user,
	ensure				=>	present,
#	gid					=>	"7777",
#	uid					=>	"7777",
	groups				=>	"sudo",
	membership			=>	inclusive,
	managehome			=>	true,
#	purge_ssh_keys		=>	true,
}

#group { $admin_user:
#	ensure				=> present,
#	allowdupe			=> false,
#	gid					=> "7777",
#}


########## SSH KEYS ##########
$admin_ssh_key = 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDOEGDj2uCcJmHFZy08Sc0UqjBGCbGfPNAGBWTPTkaRyo0tdNn3axhOcZvtcL+pD8NMsiYbmQ102VlTSOIl7cJmEQ//G09zxVXCORwBp7qUhQ1278P3i0HtQb9Cs5g6zwVDh0OKcmg0a4Z+1fitCCbiWS/eYrXhdRnfLmfoO7CIlURC5K8cLcXpdaeBtho5y8aRJj+4kd/FApaH4khw5MTYoAOXfz9GYmMsqvUWr2Z9q4ko6a9o4VEdaVwGySzB2axAT71IpZr+CX2BFa3kRgrMxXK8ATf+GScv67JA6gC7OCujJ7lKpYYRibV3w2nzexkLEDSssbIwAxCogAp9nlQ5'

ssh_authorized_key {'admin_user':
	ensure				=>	present,
	user				=>	"groot",
	type				=>	ssh-rsa,
	key					=>	$admin_ssh_key,

}

