# xubuntu1804ns

xubuntu 18.04


currently base install working against 18.04, testing with 19.04 also

# bootstrap actions needed
`sudo apt-get install puppet git puppet-module-puppetlabs-*`

`cd /etc/puppet/code`

`git clone https://gitlab.com/g1103/xubuntu1804ns.git`

`cd /etc/puppet/code/xubuntu1804ns/modules/ubuntu_base/manifests`

`puppet apply ubuntu-base.pp`


# Post Install GUI actions
installed google-chrome manually, set as default
`apt-get install terminator geany geany-common geany-plugin* keepass2 arandr`

`apt-get remove xfce4-terminal firefox`


# todo
-> set up cron job for puppet apply
-> set up cron to poll repo
-> ssh keys + update 0600



